This package contains TWO new themes for HypnOS (and matching wallpapers!):
* Classic
* Classic Candy

Installation:
	1. Copy the 'data' directory into your 'Hypnospace Outlaw' installation directory (Example on Windows: "C:\Program Files (x86)\Steam\steamapps\common\Hypnospace Outlaw\")
	2. Locate your save game file (Example on Windows: "C:\Users\<USERNAME>\Tendershoot\HypnOS\Enforcer1.sav")
	3. Make a backup copy of your save game
	4. Open your save game file and make the following changes:
		1) Append 'classic.png|classic_candy.png' onto the end of the 'UnlockedWallpapers' list
		2) Append 'Classic|Classic Candy' to onto the end of the 'UnlockedThemes' list
		3) Save your modified save game file
	5. Load up HypnOS and load your saved game, and you should have access to the two themes (plus wallpepers) in the HypnOS settings.